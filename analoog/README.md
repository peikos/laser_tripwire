# Analoge Laser-tripwire

Deze oplossing gebruikt geen microcontroller maar analoge compenten op een custom PCB.

## Ontwerp PCB
![](schematic.svg)
![](render.png)

## Benodigdheden
- PCB (bijgevoegd)
- LDR ([GL12528](https://datasheetspdf.com/pdf-file/1099855/ETC/GL12528/1), 12mm)
- Transistor ([BC550](https://nl.mouser.com/datasheet/2/308/1/BC550_D-2310266.pdf))
- Active Buzzer
- USB-B poort (voor power)
- 10KΩ weerstand
- 1KΩ weerstand

## Werking
De schakeling bevat twee aparte circuits: een met de LDR, en een tweede met de buzzer. De transistor vormt de verbinding. Het eerste circuit geeft signaal dat eerst door e 10KΩ weerstand wordt begrensd. Dit signaal kan vervolgens twee kanten op: via de LDR naar de grond, of via een constante 1KΩ weerstand naar de base van de transistor. Als de LDR voldoende licht opvangt wordt de weerstand laag (enkele honderden ohm) en zal het signaal naar de grond vloeien zonder de transistor te activeren. Als de [LDR](http://lednique.com/opto-isolators-2/light-dependent-resistor-ldr/) verduisterd wordt (doordat de laserstraal die erop gericht staat wordt doorbroken) zal de weerstand van de LDR snel oplopen tot enkele MΩ waardoor het signaal via de 1KΩ resistor naar de base van de transistor vloeit. Op dit moment sluit de transistor het tweede circuit, dat de active buzzer met de spanningsbron verbind waardoor het alarm afgaat.

## Bronnen
- [Peter Vis: Transistor as a Switch Using LDR](https://www.petervis.com/GCSE_Design_and_Technology_Electronic_Products/Transistor_as_a_Switch/Transistor_as_a_Switch_Using_LDR.html)
- [Circuitstate: How to Get Your KiCad PCB Design Ready for Fabrication – KiCad Version 6 Tutorial](https://circuitstate.com/tutorials/how-to-get-your-kicad-pcb-design-ready-for-fabrication-kicad-version-6-tutorial/)
