# Arduino gebaseerde tripwire
Deze versie gebruikt een Arduino en een mini-breadboard (via een proto-shield) om de componenten met elkaar te verbinden.

## Benodigdheden
- Arduino
- Shield met breadboard
- LDR
- Weerstand (1KΩ?)
- Active Buzzer

## Code
De code is te vinden in [tripwire.ino](tripwire.ino).

## Aansluiting
![](wiring_inc_buzzer.jpg)
![](wiring_large_ldr.jpg)
