int ldrPin = A0;
int buzzPin = 13;
bool buzz = false;
int value = 0;
int threshold = 0;

void setup()
{ pinMode(buzzPin, OUTPUT);
  digitalWrite(buzzPin, LOW); }

void loop() {
  // read input pin
  value = analogRead(ldrPin);

  // if value > threshold for two 0,5s intervals
  if (buzz && value > threshold)
  { digitalWrite(buzzPin, HIGH); }
  else
  { digitalWrite(buzzPin, LOW); }

  // save state for next iteration
  if (value > threshold)
  { buzz = true; }
  else
  { buzz = false; }

  delay(500); }
