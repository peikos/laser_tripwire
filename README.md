# Laser Tripwires

Deze repository bevat ontwerpen en (scouting) spellen rond de laser-tripwire, waarbij deelnemers een mission-impossible style uitdaging aan kunnen gaan.

## Inhoudsopgave
- [Foto's en code voor een Arduino based oplossing](arduino)
- [Schematische tekening en PCB ontwerp voor een low-cost oplossing zonder microcontrollers](analoog)

## Contributors
- Brian van der Bijl (Rover Crofts Groep / WSJ23 Troep Stadsreus)
